package com.bigdata.animal;

import com.bigdata.animal.AnimalProtoBuf.AnimalMessage;
import io.confluent.kafka.serializers.protobuf.KafkaProtobufDeserializer;
import io.confluent.kafka.serializers.protobuf.KafkaProtobufDeserializerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.serialization.StringDeserializer;

import static io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.Arrays;

public class AnimalProtobufConsumer {

    public static void main(String[] args) {
        AnimalProtobufConsumer consumer = new AnimalProtobufConsumer();
        consumer.readMessage();
    }

    public void readMessage() {
        //set consumer properties
        Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaProtobufDeserializer.class);
        //for protobuf a group id is required
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, "consumer-group");
        //protobuf requires a registry server
        properties.put(SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
        //set the Protobuf message, otherwise a generic DynamicMessage is received
        properties.put(KafkaProtobufDeserializerConfig.SPECIFIC_PROTOBUF_VALUE_TYPE, AnimalMessage.class.getName());

        //ToDo: create the consumer and subscribe

        Duration timeout = Duration.ofMillis(100);
        try {
            while (true) {
                // ToDo: receive records, consume and print age and name
                // https://kafka.apache.org/26/javadoc/org/apache/kafka/clients/consumer/ConsumerRecords.html
                // https://kafka.apache.org/26/javadoc/org/apache/kafka/clients/consumer/ConsumerRecord.html
                
                
                consumer.commitSync();
            }
        }
        finally 
        {
            consumer.close();
        }
    }
}

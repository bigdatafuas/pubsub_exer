package com.bigdata.animal;

import com.bigdata.animal.AnimalProtoBuf.AnimalMessage;
import io.confluent.kafka.serializers.protobuf.KafkaProtobufSerializer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import static io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG;

import java.util.Properties;

public class AnimalProtobufProducer {

    public static void main(String[] args) {
        AnimalProtobufProducer protobufProducer = new AnimalProtobufProducer();
        protobufProducer.writeMessage();
    }

    public void writeMessage() {
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaProtobufSerializer.class);
        properties.put(SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");

        Animal animal = new Animal(7, "Bella");

        //ToDo: create a Producer with variable name producer, see https://kafka.apache.org/26/javadoc/index.html?org/apache/kafka/clients/producer/KafkaProducer.html

        //ToDo: create a ProducerRecord with variable name animal_record and the topic protobuf-animal

        producer.send(animal_record);
        producer.flush(); //ensures record is sent before closing the producer
        producer.close();
    }
}

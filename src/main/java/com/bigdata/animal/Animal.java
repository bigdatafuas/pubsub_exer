package com.bigdata.animal;

import com.bigdata.animal.AnimalProtoBuf.AnimalMessage;

public class Animal {
    private int age;
    private String animalName;
    
    public Animal(int age, String name) {
        this.age = age;
        this.animalName = name;
    }
   
    public int getAge() {
        return age;
    }
    
    public String getName() {
        return animalName;
    }

    /**
     * Returns the protobuf serialization
     */

    public AnimalMessage getAnimalMessage() {
        // ToDo: build the message with the auto-generated Builder
        // see https://developers.google.com/protocol-buffers/docs/javatutorial
    }
} 
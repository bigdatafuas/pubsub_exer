# Introduction

This project gives an introduction to Kafka serialization and de-serialization with protobuf.

Inspired by https://github.com/codingharbour/kafka-protobuf

# Build

We use Maven to download and install required packages for this project.

During compilation also the protobuf message is compiled and Java classes including the message build is generated.

To build the project run

```
mvn clean generate-sources
mvn compile
```
in the main project directory.

# Run
If you use the provided template the kafka service and schema registry service is already running. 

If you want to build it on your own, start Kafka and the schema registry, e.g. use single-node-avro-kafka example https://github.com/codingharbour/kafka-docker-compose

To run the application, first start the consumer. Execute in the project directory
```
mvn exec:java -Dexec.mainClass=com.bigdata.animal.AnimalProtobufConsumer
```
and start the producer in a second terminal with
```
mvn exec:java -Dexec.mainClass=com.bigdata.animal.AnimalProtobufProducer
```

# Delete schema registry
If a schema is registered once you may receive the error
```
Schema being registered is incompatible with an earlier schema
```
so you have to delete the current schema. In our example use the following commands in the terminal:
```
curl -X DELETE http://localhost:8081/subjects/protobuf-animal-value
curl -X DELETE http://localhost:8081/subjects/protobuf-animal-value?permanent=true
```
It triggers a HTTP delete request on the schema `protobuf-animal-value` to schema registry server running on port 8081.

In an advanced setup you may also provide different version on a schema for backward compatibility. Here, we simply delete the current version.


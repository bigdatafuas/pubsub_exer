echo "####### IP"
ip addr > id.log
echo "####### MID"
cat /etc/machine-id > id.log

echo "Geben Sie Ihren PIN an:"
read pin

echo "Geben Sie den Upload-Server an:"
read server

tf=$(mktemp /tmp/kafka_${pin}.XXXXXXXXXXX.tgz)
cd ..
tar cfvz ${tf} kafka_template
cd kafka_template

btf=$(basename $tf)

upload () {
  export SSHPASS=student
  sshpass -e sftp -oBatchMode=no -b - students@{server} << !
   cd upload
   put ${tf}
   ls ${btf}
   bye
!
}

res=$(upload)

check=$(echo $res | grep "ls ${btf} ${btf}")
if [[ $check == *"${btf}"* ]]; then
  echo "Dateien wurden hochgeladen."
else
  echo "Aufgabe nicht abgegeben!"
fi
